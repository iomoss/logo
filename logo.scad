$fn = 25;

module logo_tile(up = false, down=false, left=false, right=false, mid=true, inverse=true)
{
    hole_size=5/2;
    line_width=hole_size;

    difference()
    {
        if(inverse)
            square([10,10], center=true);

        union() 
        {
            if(mid)
                circle(r=hole_size);
            if(up)
                translate([0, 10/2])
                    square([line_width,10], center=true);
            if(down)
                translate([0, -10/2])
                    square([line_width,10], center=true);
            if(left)
                translate([-10/2,0])
                    square([10,line_width], center=true);
            if(right)
                translate([10/2,0])
                    square([10,line_width], center=true);
        }
    }
}

module logo_tile_double(up = false, down=false, left=false, right=false, mid=true)
{
    logo_tile(up, down, left, right, up);
    translate([0,10])
        logo_tile(up, down, left, right, up);
}

module I()
{
    logo_tile_double(true,true);
    translate([10,0])
        logo_tile_double(true,true);
}

module O()
{
    translate([10,0])
        logo_tile(true);
    translate([10,10])
        logo_tile(false, true);
}

module M()
{
    logo_tile_double(true,true);
    translate([10,0])
        logo_tile(true, true);
    translate([10,10])
        logo_tile(false, true);
    translate([20,0])
        logo_tile(true, true);
    translate([20,10])
        logo_tile(false, true);
    translate([30,0])
        logo_tile_double(true,true);
}

module S()
{
    logo_tile(false, false, true, true);
    translate([0,10])
        logo_tile(false, false, false, true);
    translate([10,0])
    {
        logo_tile(false, false, true);
        translate([0,10])
            logo_tile(false, false, true, true);
    }
}

module IOMOSS_inline()
{
    I();
    translate([10,0])
        O();
    translate([30,0])
        M();
    translate([60,0])
        O();
    translate([80,0])
        logo_tile(true,true,false,true);
    translate([80,10])
        logo_tile(true,true);
    translate([90,0])
        S();
    translate([110,0])
        logo_tile(true,true,false,true);
    translate([110,10])
        logo_tile(true,true,true);
    translate([120,0])
        S();
    translate([140,0])
    {
        logo_tile(true,true);
        translate([0,10])
            logo_tile(true,true,true);
    }
}

module outline()
{
    translate([0,-10])
        logo_tile(true,false,true,true);

    translate([0,20])
        logo_tile(false,true,true,true);
}

module outline_no_left()
{
    translate([0,-10])
        logo_tile(true,false,false,true);

    translate([0,20])
        logo_tile(false,true,false,true);
}

module outline_no_right()
{
    translate([0,-10])
        logo_tile(true,false,true,false);

    translate([0,20])
        logo_tile(false,true,true,false);
}

module outline_no_in()
{
    translate([0,-10])
        logo_tile(false,false,true,true);

    translate([0,20])
        logo_tile(false,false,true,true);
}

module outline_bot_in()
{
    translate([0,-10])
        logo_tile(true,false,true,true);

    translate([0,20])
        logo_tile(false,false,true,true);
}

module IOMOSS_outline()
{
    outline_no_left();
    translate([10,0])
        outline();
    translate([20,0])
        outline_no_in();
    translate([30,0])
        outline();
    translate([40,0])
        outline_bot_in();
    translate([50,0])
        outline_bot_in();
    translate([60,0])
        outline();
    translate([70,0])
        outline_no_in();
    translate([80,0])
        outline();
    translate([90,0])
        outline_no_in();
    translate([100,0])
        outline_no_in();
    translate([110,0])
        outline();
    translate([120,0])
        outline_no_in();
    translate([130,0])
        outline_no_in();
    translate([140,0])
        outline_no_right();
}

module IOMOSS()
{
    intersection()
    {
        translate([0,-5])
            union()
            {
                IOMOSS_inline();
                IOMOSS_outline();
            }
        translate([140/2,0])
            square([140,30], center=true);
    }
}

linear_extrude(5)
    IOMOSS();
